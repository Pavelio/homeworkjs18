"use strict";
function isCheckString(string, name) {
    while ((!string)||(string == null)||(!isNaN(string))) {
        string = prompt(`Enter your ${name}:`, string);
    }
    return string;
}
function isCheckNumber(number, name) {
    while ((!number)||((number<1)||(number>10))||(isNaN(number))) {
        number = +prompt(`Enter your ${name}:`, number);
    }
    return number;
}
function checkTabelStudent() {
    let student = {
        name: isCheckString( prompt('Enter your first name:'), "first name"),
        ['last name']: isCheckString( prompt('Enter your last name:'), "last name"),
    };
    let tabel = new Map();
    let controlPoint = 0;
    let sum = 0;
    let getSubject = "undefined";
    while (getSubject!=null) {
        getSubject = prompt('Enter your subject:');
        if(getSubject == null) break;
        getSubject = isCheckString(getSubject, 'subject');
        let getGrade = isCheckNumber( +prompt('Enter your grade:'), "grade");
        student.tabel = tabel.set(getSubject, Number(getGrade));
        if(tabel.get(getSubject)<4) {
            controlPoint++;
        }    
        sum+= tabel.get(getSubject);
    }
    let gradePointAverage = sum/tabel.size;
    if(gradePointAverage>=7) console.log(`Студенту назначена стипендия, средний балл = ${gradePointAverage}`);
    if(controlPoint==0) console.log("Студент переведен на следующий курс");
    else console.log(`Студент не переведен на следующий курс, плохих оченок: ${controlPoint}`);
    return student;
}
let student = new checkTabelStudent();
console.log(student);